QT.json.VERSION = 1.0.0
QT.json.MAJOR_VERSION = 1
QT.json.MINOR_VERSION = 0
QT.json.PATCH_VERSION = 0

QT.json.name = QtJson
QT.json.bins = $$QT_MODULE_BIN_BASE
QT.json.includes = $$QT_MODULE_INCLUDE_BASE $$QT_MODULE_INCLUDE_BASE/QtJson
QT.json.private_includes = $$QT_MODULE_INCLUDE_BASE/QtJson/$$QT.json.VERSION
QT.json.sources = $$QT_MODULE_BASE/src
QT.json.libs = $$QT_MODULE_LIB_BASE
QT.json.plugins = $$QT_MODULE_PLUGIN_BASE
QT.json.imports = $$QT_MODULE_IMPORT_BASE
QT.json.depends = core

QT_CONFIG += json
