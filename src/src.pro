TEMPLATE = lib
TARGET = $$QT.json.name
MODULE = json

load(qt_module)
load(qt_module_config)

DESTDIR = $$QT.json.libs
VERSION = $$QT.json.VERSION
DEFINES += QT_JSON_LIB

QT = core

CONFIG += module create_prl
MODULE_PRI = ../modules/qt_json.pri

HEADERS += qtjsonversion.h

HEADERS += \
    qjson_p.h \
    qjsondocument.h \
    qjsonobject.h \
    qjsonglobal.h \
    qjsonvalue.h \
    qjsonarray.h \
    qjsonwriter_p.h \
    qjsonparser_p.h

SOURCES += \
    qjson.cpp \
    qjsondocument.cpp \
    qjsonobject.cpp \
    qjsonarray.cpp \
    qjsonvalue.cpp \
    qjsonwriter.cpp \
    qjsonparser.cpp

mac:QMAKE_FRAMEWORK_BUNDLE_NAME = $$QT.json.name
